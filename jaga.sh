#!/bin/bash
#Marko Mandel
#Skript jagab etteantud grupile uue kausta
#

#kui ei ole korrektne parameetrite arv, siis kuva 'help' message, ja v2lju


if [ $UID -ne 0 ]; then
    echo "K2ivita skript juurkasutaja 6igustes"
    exit 1
fi

#Kontrollib kas v6tmete arv on 2 v6i 3
if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <JAGATUD KAUST>"
    exit 1
fi

KAUST=$1
KAUSTBASE=$(basename $KAUST)
GRUPP=$2               
JAGAKAUST=${3-$KAUSTBASE}


#Kui tee ei hakka slashiga, siis on failitee suhteline, ja see tehakse absoluutseks 
#kui hakkas tee punktiga, siis punkt eemaldatakse
if [[ ${KAUST:0:1} != "/" ]]; then
   if [[ ${KAUST:0:2} == "./" ]]; then
       KAUST=${KAUST:1}
       KAUST="$(pwd)$KAUST"
   else
       KAUST="$(pwd)/$KAUST"
   fi
fi


#Kontrollib, kas samba on paigaldatud. Kui ei ole, paigaldab.
type smbd > /dev/null 2>&1 

if [ $? -ne 0 ]; then
    echo "Paigaldan Samba..."    
    apt-get update > /dev/null 2>&1 && apt-get install samba -y > /dev/null 2>&1 || exit 1
fi

#Kontrollib kas kaust on olemas, vajadusel loob.
if [ ! -d "$KAUST" ]; then
    mkdir -p "$KAUST"  > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        echo "Viga kausta loomisel"   
        exit 4   
    fi 
fi



#Kontrollib kas grupp on olemas, vajadusel loob selle.
getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null


#Kopeerin konfiguratsiooni-faili ymber
echo "Konfigureerin Samba..."
cp /etc/samba/smb.conf /etc/samba/smb.conf.old


#samba konfi faili koopia muutmine
cat >> /etc/samba/smb.conf.old  <<LOPP                      
[$JAGAKAUST]                              #jagatud kaust
    comment=Jagatud kaust
    path=$KAUST                 
    writable=yes
    valid users=@$GRUPP                         
    force group=$GRUPP                             
    browsable=yes
    create mask=0664
    directory mask=0775
LOPP

#kontrollib kas konfiguratsiooni-fail on korrektne (Pole /dev/null-i suunatud)
testparm -s /etc/samba/smb.conf.old > /dev/null 2>&1


#kui konfiguratsioonifail oli korras, siis kopeerib koopia, 6ige faili asemele.
if [ $? -eq 0 ]; then
   echo "Syntaks on korras"
   cat /etc/samba/smb.conf.old > /etc/samba/smb.conf 
else
   echo "Viga: samba konfiguratsioonifail on vigane"
   exit 4                    
fi


echo "K2ivitan Samba..."

sudo /etc/init.d/samba reload

echo "Valmis"


