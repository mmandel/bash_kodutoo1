#!/bin/bash
#Marko Mandel
#Skript jagab etteantud grupile uue kausta
#

#kui ei ole korrektne parameetrite arv, siis kuva help message, ja v2lju
if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <KAUST GRUPP>"
    exit 1
fi

KAUST=$1
KAUSTBASE=$(basename $KAUST)
GRUPP=$2               #siin oli enne KAUST=$2
JAGAKAUST=${3-$KAUSTBASE}

if [ $# -ne 2 ] && [ $# -ne 3 ]; then
    echo "Kasutamine: $0 KAUST GRUPP <KAUST GRUPP>"
    exit 1
fi


#if ! smbd > /dev/null; then
  #  echo "Paigaldan Samba...>"    
  #  apt-get update > /dev/null 2>&1 && apt-get install samba -y > /dev/null || exit 1
#fi
echo "Samba on peal" 

if [ ! -d "$KAUST" ]; then
    echo "Loon directory" 
    mkdir -p "$KAUST"
fi

getent group $GRUPP > /dev/null || addgroup $GRUPP > /dev/null
#> /dev/null
echo "loongrupi" 
getent group $GRUPP

cp /etc/samba/smb.conf /etc/samba/smb.conf.old

echo "Konfigureerin Samba..."
#samba konfi faili koopia muutmine
sudo bash -c "cat >> /etc/samba/smb.conf.old"  <<LOPP                      
[$KAUSTBASE]                              #jagatud kaust
 comment=Jagatud kaust
 path=$KAUST                 
 writable=yes
 valid users=@$GRUPP                         
 force group=$GRUPP                             
 browsable=yes
 create mask=0664
 directory mask=0775
LOPP

sudo bash -c "testparm -s /etc/samba/smb.conf.old > /etc/samba/smb.conf"

sudo /etc/init.d/samba reload

#samba konfi fail***

#konfifaili pane kommentaar ka sisse



#igaksjuhuks teeb konfi failist koopia  -ei tea kas tootab ilma sudota
#cp /etc/samba/smb.conf /etc/samba/smb.conf.old


#samba konfi fail***

#sudo bash -c "cat >> /etc/samba/smb.conf.old"  <<LOPP                 
#[lab]                              #jagatud kaust
# comment=Jagatud kaust
# path=$KAUST                 
# writable=yes
# valid users=@$GRUPP                         
# force group=$GRUPP                             
# browsable=yes
# create mask=0664
# directory mask=0775
#LOPP






#echo kanasott >> /etc/samba/pask.txt
#sudo cat >> /etc/samba/pask.txt <<LOPP                          
#kanasitt
#sdfsdfsdff
#sdfsdf
#LOPP

#
#nano /etc/samba/smb.conf.old  





#samba konfifaili tuleb faili taispikk tee teha(BASHI MATERJALIDE SLAID 41)


#resa kaib probs sudo /etc/init.d/samba reload


echo "KAUST=$KAUST"
echo "KAUSTBASE=$KAUSTBASE"
echo "GRUPP=$GRUPP"
echo "JAGAKAUST=$JAGAKAUST"
echo $4

